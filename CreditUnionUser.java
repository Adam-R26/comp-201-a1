/*
 * COMP201 2019-20
 * Assignment 1
 * CreditUnionUser.java
 * NAME: Adam Ray
 * STUDENT ID: 201271021
 * COMPUTER USERNAME: psaray 
 */
 
 
 //TODO: You must go through ALL CODE to ensure it meets requirements!!!
 
 public class CreditUnionUser{

	 public static void main(String[] args) throws AccountException
	 {
			//Create new Clerk
			Clerk clerk = new Clerk("Donald");
			
			//Create some new customers
			Customer customer1 = new Customer("Thomas");
			Customer customer2 = new Customer("Richard");
			Customer customer3 = new Customer("Harry");
			Customer customer4 = new Customer("Sally");
			
			//Create the credit union with opening capital
			CreditUnion cu = new CreditUnion(10000.00, clerk);
			System.out.println("Credit Union Capital is: " + cu.getTotalCapital(clerk));

			//TODO: Write code that tests the program. Use the code below to prompt your testing
			
			
			//Create some saving accounts (The Clerk must do this on behalf of the customer)

			clerk.openSavings(customer1, 300.0);
			System.out.println("Credit Union Capital is: " + cu.getTotalCapital(clerk));
			
			customer1.makeWithdrawal(10.0);
			System.out.println("Credit Union Capital is: " + cu.getTotalCapital(clerk));
			
			customer1.makePayment(100);
			System.out.println("Credit Union Capital is: " + cu.getTotalCapital(clerk));
			clerk.closeAccount(customer1);
			System.out.println("Credit Union Capital is: " + cu.getTotalCapital(clerk));
			
			//Add balances to saving accounts (The customer does this)
			
			clerk.openSavings(customer3,1000);
			
			//Make some withdrawals from saving accounts (The customer does this)
			customer3.makeWithdrawal(200);
			
			//Process a loan application (The clerk does this on behalf of the customer)
			
			
			clerk.openLoan(customer2, -200);
			clerk.openLoan(customer4, -1000);
			
			
			//Make a payment to loan account (The customer does this)
			customer2.makePayment(200);
			
			//Close loan account (The clerk does this)
			clerk.closeAccount(customer2);
			
			//Close saving accounts (The clerk does this)
			clerk.closeAccount(customer3);
			
			//Testing the applyInterest method
			clerk.applyInterest();
			
			
			
			
			
	 } 
	 
};
