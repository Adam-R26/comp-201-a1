/*
 * COMP201 2019-20
 * Assignment 1
 * Clerk.java *V2*
 * NAME: Adam Ray
 * STUDENT ID: 201271021
 * COMPUTER USERNAME: psaray
 */
 
 public class Clerk extends Person{
	private CreditUnion union;
	 
	 public Clerk()
	 {
		 super(); 
	  }
	 
	 public Clerk(CreditUnion theUnion)
	 {
		 super();
		 union = theUnion;
	 }
	 
	 public Clerk(String theName)
	 {
			super(theName);
	 }
	 
	 public void setUnion(CreditUnion theUnion)
	 {
			union = theUnion;
	 }
	 
	 
	 public void addAccount(Account theAccount)
	{

		union.accounts.add(theAccount);
		
	}
	
	public void rmAccount(Account theAccount)
	{
		union.accounts.remove(theAccount);
	}
	
	public void addCustomer(Customer c)
	{
		union.people.add(c);
		System.out.println("Customer " + c.name + " is registered with the Credit Union");
	}
	
	public void rmCustomer(Customer c)
	{
		union.people.remove(c);
	}
	 
	 public boolean openSavings(Customer theCustomer, double openAmount)
	 {
		 //Check the Customer is registered with the union
		 if(!(union.people.contains(theCustomer)))
		 {
			 addCustomer(theCustomer);
		 }
		 
			 //Check we are opening the account with a valid amount
			 if(openAmount >=0.0)
			 {
				 try
				 {
					//Create a Saving account for the customer with the openAmount
					Account theAccount = new Savings(openAmount, theCustomer); 
					addAccount(theAccount);
					return true;
				 }catch(AccountException e)
				 {
						System.out.println("Account Exception caught whilst opening a savings account");
				 }
				
			 }
		 
		 return false;
	 }
	 
	 public boolean openLoan(Customer theCustomer, double theAmount)
	 {
		if(union.processLoanApplication(this,theAmount)){//Check person handling loan is valid and the amount is feasible.
			if(!(union.people.contains(theCustomer))){//Check if customer already registered
				addCustomer(theCustomer);//If not register customer
				if(theAmount <= 0){//If the amount is in correct format.
					 try{
						 //Create a Loan account for the customer with balance theAmount
						 Account theAccount = new Loan(theAmount,theCustomer);
						 addAccount(theAccount);
						 return true;
					 }catch(AccountException e){
						 System.out.println("Account Exception caught while opening a loan account");
					 }
				 }
			}
		}
		else{
			System.out.println("Sorry a loan is not availiable for you at the moment.");
		}
			 return false;
	 
	 }
	 
	 public boolean closeAccount(Customer theCustomer)
	 {
		 //TODO: Complete this method
		 if(!(union.people.contains(theCustomer))){//Check is customer is registered with an account.
			 System.out.println("Cannot close account for customer that doesn't exist.");
		 }
		 else{//If customer has account do close said account, and remove it from the vector.
			try{
			Account theAccount = theCustomer.myAccount;
			theAccount.close();
			rmCustomer(theCustomer);
			rmAccount(theCustomer.myAccount);
			}catch(AccountException e){
				System.out.println("Account Exception caught while closing account.");
				return false;
			}
		 }
		 
		 return true;
	 }
	 
	 public boolean applyInterest()
	{	//TODO: Complete this method
		for(int i = 0; i<union.accounts.size(); i++){//Go through all of the registered accounts
			if(union.accounts.elementAt(i) instanceof Loan){//If a registered account is a loan account
				double Balance = union.accounts.elementAt(i).getBalance();
				union.accounts.elementAt(i).setBalance(Balance*1.01);//Apply interest
				System.out.println("Interest applied to the loan account of:"+union.accounts.elementAt(i).holder.getName());
				if(i == (union.accounts.size()-1)){
					return true;//After last account ammended return true to show applying interest was successful.
				}
			}
		}
		return false;
	}
};
